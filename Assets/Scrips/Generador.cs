﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Generador : MonoBehaviour
{

    public Transform Padre;
    //private GameObject Hijo;
    private bool B = false;

    public GameObject prefabObjeto;


    void Start()
    {
       // Generar();

    }

    // Update is called once per frame
    void Update()
    {

        Manager();
        
    }

    Vector3 DarPos()
    {
        float posX = Random.Range(-9, 10);
        float posY = Random.Range(1, 5);
        float posZ = Random.Range(4, 20);

        Vector3 posRandom = new Vector3(posX, posY, posZ);

        return posRandom;
    }

    public void Generar()
    {
        for (int i = 0; i < 5; i++)
        {
            Instantiate(prefabObjeto, DarPos(), prefabObjeto.transform.rotation);
        }
    }

    public void Papa(Transform newParent) 
    {
        prefabObjeto.transform.SetParent(newParent);

    }

    public void Manager()
    {
        if (transform.childCount==0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }




}
