﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mirar : MonoBehaviour
{
    public float mouseMove = 700f;
    public Transform playerBody;
    float xRotation = 0;
    void Start()
    {
        
    }
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseMove * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseMove * Time.deltaTime;
        xRotation -= mouseY;

        xRotation = Mathf.Clamp(xRotation, -90f, 90);
        transform.localRotation = Quaternion.Euler(xRotation,0f,0f);
        playerBody.Rotate(Vector3.up * mouseX);


    }
}
