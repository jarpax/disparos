﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{

    private Rigidbody rbEnemigo;
    private bool B = true;
    void Start()
    {
        rbEnemigo = GetComponent<Rigidbody>();
    }

    void Update()
    {

        if (B == true)
        {


            Move();
            B = false;
            StartCoroutine(Esperar());
            
        }
    }

    Vector3 DarPos()
    {
        float posX = Random.Range(-9, 10);
        float posY = Random.Range(1, 5);
        float posZ = Random.Range(4, 20);

        Vector3 posRandom = new Vector3(posX, posY, posZ);

        return posRandom;
    }
    
    private void Move()
    {
        rbEnemigo.AddForce((transform.position = DarPos()).normalized * 0.5f);
    }

    IEnumerator Esperar()
    {

        yield return new WaitForSeconds(1.5f);
        B = true;
        

    }
}
